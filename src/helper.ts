import axios from 'axios';
import { SERVICE_TOKEN, TIMEOUT } from '@kominal/service-util/helper/environment';
import { GroupDatabase } from '@kominal/connect-models/group/group.database';
import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';
import { error, debug } from '@kominal/lib-ts-logging';
import { Profile } from '@kominal/ecosystem-user-service/models/profile/profile';
import { ECOSYSTEM_STACK_NAME } from './environment';
import StatusDatabase from './models/status.database';
import { service } from '.';
import { OutboundEvent } from '@kominal/ecosystem-models/event.outbound';

export async function getGroup(groupId: string): Promise<any> {
	const group = await GroupDatabase.findById(groupId);

	if (!group) {
		throw 'error.group.notfound';
	}

	return group.toJSON();
}

export async function getMembership(groupId: string, userId: string): Promise<any> {
	const membership = await MembershipDatabase.findOne({ userId, groupId });

	if (!membership) {
		throw 'error.group.notmember';
	}

	return membership.toJSON();
}

export async function verifyProfileOwnership(userId: string, profileId: string): Promise<boolean> {
	try {
		const { data, status } = await axios.get<Profile>(`http://${ECOSYSTEM_STACK_NAME}_user-service:3000/profile/${profileId}`, {
			headers: {
				authorization: SERVICE_TOKEN,
				userId,
			},
			timeout: TIMEOUT,
		});
		console.log(data);
		if (data && status == 200) {
			return data.userId === userId;
		}
	} catch (e) {
		error(e);
	}
	return false;
}

export async function getStatus(userId: string) {
	const statusSetting = await StatusDatabase.findOne({ userId });
	return statusSetting?.get('status') || 'ONLINE';
}

export async function publishStatusUpdate(userId: string, status: string) {
	debug(`Sending ${status} for ${userId} to relvant users...`);
	const groupIds = await MembershipDatabase.find({ userId, left: null, removed: null }).distinct('groupId');
	const userIds = await MembershipDatabase.find({
		userId: { $ne: userId },
		left: null,
		removed: null,
		groupId: { $in: groupIds },
	}).distinct('userId');

	await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
		headers: {
			userIds,
		},
		type: 'connect.status.changed',
		content: {
			userId,
			status,
		},
	} as OutboundEvent);
}
