import { ConsumeMessage, ConfirmChannel } from 'amqplib';
import { warn, debug } from '@kominal/lib-ts-logging';
import { InboundEvent } from '@kominal/ecosystem-models/event.inbound';
import { SocketConnectionDatabase } from './models/socketconnection.database';
import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';
import { service } from '.';
import { getStatus, publishStatusUpdate } from './helper';
import { OutboundEvent } from '@kominal/ecosystem-models/event.outbound';

export async function onSyncMessage(channel: ConfirmChannel, msg: ConsumeMessage | null): Promise<void> {
	if (!msg) {
		warn('Received outbound message without content.');
		return;
	}

	debug('Handling ecosystem sync message...');

	const { type, content } = JSON.parse(msg.content.toString());

	if (type === 'ecosystem.live.tasks') {
		const { taskIds } = content;

		const socketConnections = await SocketConnectionDatabase.find({ taskId: { $not: { $in: taskIds } } });
		for (const socketConnection of socketConnections) {
			const { connectionId, userId } = socketConnection.toJSON();
			await onUserDisconnect(connectionId, userId);
		}

		if (socketConnections.length > 0) {
			warn(`Removed ${socketConnections.length} dead socket connections during ecosystem sync!`);
		}
	}

	channel.ack(msg);
}

export async function onMessage(channel: ConfirmChannel, msg: ConsumeMessage | null): Promise<void> {
	if (!msg) {
		warn('Received outbound message without content.');
		return;
	}

	const { headers, type, content }: InboundEvent = JSON.parse(msg.content.toString());

	const { taskId, connectionId, userId } = headers as any;

	if (type === 'ecosystem.client.connect') {
		await onUserConnect(taskId, connectionId, userId);
	} else if (type === 'ecosystem.client.disconnect') {
		await onUserDisconnect(connectionId, userId);
	} else if (type === 'connect.call.join') {
		onCallJoin(connectionId, userId, content);
	} else if (type === 'connect.call.signaling') {
		onCallSignaling(connectionId, userId, content);
	} else if (type === 'connect.call.leave') {
		await onCallLeave(connectionId, userId);
	}

	channel.ack(msg);
}

async function onUserConnect(taskId: string, connectionId: string, userId: string) {
	debug(`Handling connect for ${taskId}/${connectionId}/${userId}...`);

	await SocketConnectionDatabase.create({ taskId, connectionId, userId });

	const status = await getStatus(userId);
	if (status != 'OFFLINE') {
		if ((await SocketConnectionDatabase.countDocuments({ userId })) === 1) {
			await publishStatusUpdate(userId, status);
		}

		await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
			headers: {
				connectionIds: [connectionId],
			},
			type: 'connect.status.changed',
			content: {
				userId,
				status,
			},
		} as OutboundEvent);
	}

	const groupIds = await MembershipDatabase.find({ userId, left: null, removed: null }).distinct('groupId');

	const userIds = await MembershipDatabase.find({
		userId: { $ne: userId },
		left: null,
		removed: null,
		groupId: { $in: groupIds },
	}).distinct('userId');

	for (const userId of userIds) {
		const status = await getStatus(userId);
		if (status != 'OFFLINE' && (await SocketConnectionDatabase.countDocuments({ userId })) > 0) {
			await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
				headers: {
					connectionIds: [connectionId],
				},
				type: 'connect.status.changed',
				content: {
					userId,
					status,
				},
			} as OutboundEvent);
		}
	}

	for (const groupId of groupIds) {
		const members: any[] = [];
		for (const socketConnection of await SocketConnectionDatabase.find({ groupId })) {
			members.push({
				connectionId: socketConnection.get('connectionId'),
				userId: socketConnection.get('userId'),
			});
		}

		if (members.length > 0) {
			await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
				headers: {
					connectionIds: [connectionId],
				},
				type: 'connect.call.members',
				content: {
					groupId,
					members,
				},
			});
		}
	}
}
async function onUserDisconnect(connectionId: string, userId: string) {
	await onCallLeave(connectionId, userId);

	await SocketConnectionDatabase.deleteMany({ connectionId });

	const status = await getStatus(userId);

	if (status != 'OFFLINE' && (await SocketConnectionDatabase.countDocuments({ userId })) === 0) {
		await publishStatusUpdate(userId, 'OFFLINE');
	}
}

async function onCallJoin(connectionId: string, userId: string, { groupId }: any) {
	await onCallLeave(connectionId, userId);

	let type = 'connect.call.started';
	if ((await SocketConnectionDatabase.countDocuments({ groupId })) > 1) {
		type = 'connect.call.joined';
	}

	await SocketConnectionDatabase.updateOne({ connectionId }, { groupId });

	const memberships = await MembershipDatabase.find({ groupId });
	const userIds = memberships.map((m) => m.get('userId'));

	await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
		headers: {
			userIds,
		},
		type,
		content: {
			groupId,
			connectionId,
			userId,
		},
	});
}

async function onCallSignaling(
	connectionId: string,
	userId: string,
	content: {
		targetConnectionId: string;
		signaling: string;
	}
) {
	const { targetConnectionId, signaling } = content;

	const socketConnection = await SocketConnectionDatabase.findOne({ connectionId });
	if (socketConnection) {
		const groupId = socketConnection.get('groupId');
		const targetSocketConnection = await SocketConnectionDatabase.findOne({ groupId, connectionId: targetConnectionId });
		if (targetSocketConnection) {
			await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
				headers: {
					connectionIds: [targetConnectionId],
				},
				type: 'connect.call.signaling',
				content: {
					groupId,
					connectionId,
					userId,
					signaling,
				},
			});
		}
	}
}

async function onCallLeave(connectionId: string, userId: string) {
	const socketConnection = await SocketConnectionDatabase.findOne({ connectionId });
	if (socketConnection) {
		const groupId = socketConnection.get('groupId');
		await SocketConnectionDatabase.updateOne({ connectionId }, { groupId: undefined });

		const userIds = await MembershipDatabase.find({ groupId }).distinct('userId');

		await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
			headers: {
				userIds,
			},
			type: 'connect.call.left',
			content: {
				groupId,
				connectionId,
				userId,
			},
		});
	}
}
