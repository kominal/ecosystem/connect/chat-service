import { model, Schema, Types } from 'mongoose';

const StatusDatabase = model(
	'Status',
	new Schema(
		{
			userId: Types.ObjectId,
			status: String,
		},
		{ minimize: false }
	)
);

export default StatusDatabase;
