import { model, Schema, Types } from 'mongoose';

export const SocketConnectionDatabase = model(
	'SocketConnection',
	new Schema(
		{
			taskId: String,
			connectionId: String,
			userId: Types.ObjectId,
			groupId: String,
		},
		{ minimize: false }
	)
);
