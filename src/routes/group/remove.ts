import { verifyParameter } from '@kominal/service-util/helper/util';
import { GroupDatabase } from '@kominal/connect-models/group/group.database';
import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import { getGroup, getMembership } from '../../helper';
import { service } from '../..';
import { MessageDatabase } from '@kominal/connect-models/message/message.database';
import StorageDatabase from '@kominal/connect-models/storage/storage.database';

const router = new Router();

/**
 * @group Protected
 * @security JWT
 * @route DELETE /remove
 * @consumes application/json
 * @produces application/json
 * @param {string} group.path.required - The group id.
 * @returns {void} 200 - The group was deleted.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsUser('/group/:groupId/remove', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const group = await getGroup(groupId);

	await getMembership(groupId, userId);

	const userIds = (await MembershipDatabase.find({ groupId, left: undefined })).map((m) => m.get('userId'));

	await MembershipDatabase.updateMany({ groupId, userId, left: null }, { left: Date.now() });
	await MembershipDatabase.updateMany({ groupId, userId }, { removed: Date.now() });

	res.status(200).send();

	await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
		headers: {
			userIds,
		},
		type: 'connect.group.removed',
		content: {
			userId,
			groupId,
		},
	});

	const remainingMembers = await MembershipDatabase.find({ groupId, removed: null });

	if (remainingMembers.length == 0) {
		await GroupDatabase.deleteOne({ _id: groupId });
		await MembershipDatabase.deleteMany({ groupId });

		const messages = await MessageDatabase.find({ groupId });
		const storageIds = messages.map((message) => message.get('storageId'));
		await MessageDatabase.deleteMany({ groupId });
		await StorageDatabase.deleteMany({ _id: { $in: storageIds } });
	}
});

export default router.getExpressRouter();
