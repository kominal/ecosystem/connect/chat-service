import { verifyParameter } from '@kominal/service-util/helper/util';
import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import { getMembership } from '../../helper';
import { service } from '../..';
import { Membership } from '@kominal/connect-models/membership/membership';

const router = new Router();

/**
 * Gets a list of members for a group
 * @group Protected
 * @security JWT
 * @route GET /profile/{profileId}/memberships/count
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The groupId
 * @returns {Array} 200 - A list of members
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsRoot('/profile/:profileId/inuse', async (req, res) => {
	const { profileId } = req.params;
	verifyParameter(profileId);

	const count = await MembershipDatabase.countDocuments({ profileId, left: null });

	res.status(200).send(count > 0);
});

/**
 * Notifies the group service about a profile update
 * @group Protected
 * @security JWT
 * @route GET /members/profile/{profileId}/{lastUpdated}
 * @consumes application/json
 * @produces application/json
 * @param {string} profileId.path.required - The profileId
 * @returns {Array} 200 - The request was consumed
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.putAsRoot('/members/profile/:profileId/:updatedAt', async (req, res) => {
	const { profileId, updatedAt } = req.params;
	verifyParameter(profileId, updatedAt);

	const memberships = await MembershipDatabase.find({ profileId, left: null });

	for (const membership of memberships) {
		//service.getSMQClient()?.publish('TOPIC', `DIRECT.GROUP.${membership.get('groupId')}`, 'PROFILE.UPDATED', { id: profileId, updatedAt });
	}

	res.status(200).send();
});

/**
 * Gets a list of members for a group
 * @group Protected
 * @security JWT
 * @route GET /members/{groupId}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The groupId
 * @returns {Array} 200 - A list of members
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/group/:groupId/members', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	await getMembership(groupId, userId);

	const members: Membership[] = (await MembershipDatabase.find({ groupId, left: null }))
		.map((membership) => membership.toJSON())
		.map((m) => {
			return {
				id: m._id,
				userId: m.userId,
				groupId: m.groupId,
				profileId: m.profileId,
				profileKey: m.profileKey,
				joined: m.joined,
				left: m.left,
				role: m.role,
				updatedAt: m.updatedAt,
			};
		});

	res.status(200).send(members);
});

/**
 * Update the profile of a user
 * @group Protected
 * @security JWT
 * @route POST /members/{groupId}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The groupId
 * @param {string} profileId.body.required - The profileId
 * @param {string} profileKey.body.required - The profileKey
 * @returns {Array} 200 - A new update timestamp
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/group/:groupId/membership', async (req, res, userId) => {
	const { groupId } = req.params;
	const { profileId, profileKey } = req.body;
	verifyParameter(groupId, profileId, profileKey?.data, profileKey?.iv);

	const membership = await getMembership(groupId, userId);

	if (membership.get('left') != null) {
		throw 'error.group.notamember';
	}

	const updatedAt = Date.now();

	await MembershipDatabase.updateOne({ groupId, userId }, { profileId, profileKey, updatedAt });

	//service.getSMQClient()?.publish('TOPIC', `DIRECT.GROUP.${groupId}`, 'GROUP.PROFILE.CHANGED', { groupId, userId, profileId: profileId, updatedAt });

	res.status(200).send(updatedAt);
});

/**
 * Get the membership of a group for a specific user
 * @group Protected
 * @security JWT
 * @route GET /members/{group}/{user}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The groupId
 * @param {string} partnerId.path.required - The userId of the partner
 * @param {string} lastUpdated.path.optional - The lastUpdated timestamp of the users copy of the membership
 * @returns {void} 200 - TODO
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/group/:groupId/member/:partnerId/:updatedAt*?', async (req, res, userId) => {
	const { groupId, partnerId, updatedAt } = req.params;
	verifyParameter(groupId, partnerId);

	await getMembership(groupId, userId);

	const mPartner = await getMembership(groupId, partnerId);

	if (updatedAt && mPartner.updatedAt === Number(updatedAt)) {
		res.status(304).send();
		return;
	}

	const membershipEncrypted: Membership = {
		id: mPartner._id,
		userId: mPartner.userId,
		groupId: mPartner.groupId,
		profileId: mPartner.profileId,
		profileKey: mPartner.profileKey,
		joined: mPartner.joined,
		left: mPartner.left,
		role: mPartner.role,
		updatedAt: mPartner.updatedAt,
	};

	res.status(200).send(membershipEncrypted);
});

export default router.getExpressRouter();
