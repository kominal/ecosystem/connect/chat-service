import { verifyParameter } from '@kominal/service-util/helper/util';
import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import { getGroup, getMembership } from '../../helper';
import { service } from '../..';

const router = new Router();

/**
 * Removes a user from a group
 * @group Protected
 * @security JWT
 * @route DELETE /
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.body.required - The id of the group
 * @param {string} partnerId.body.required - The id of the user to remove
 * @returns {void} 200 - The user was removed
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.deleteAsUser('/group/:groupId/:partnerId', async (req, res, userId) => {
	const { groupId, partnerId } = req.params;
	verifyParameter(groupId, partnerId);

	const g = await getGroup(groupId);

	if (g.type === 'DM') {
		throw 'error.group.cannotmodifydm';
	}

	const m = await getMembership(groupId, userId);

	if (m.role !== 'ADMIN') {
		throw 'error.group.notadmin';
	}

	const mPartner = await getMembership(groupId, partnerId);

	if (mPartner.left != null) {
		throw 'error.group.notamember';
	}

	const userIds = (await MembershipDatabase.find({ groupId, left: undefined })).map((m) => m.get('userId'));

	await MembershipDatabase.updateMany({ groupId, userId: partnerId }, { left: new Date() });

	res.status(200).send();

	await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
		headers: {
			userIds,
		},
		type: 'connect.group.left',
		content: {
			groupId,
		},
	});
});

export default router.getExpressRouter();
