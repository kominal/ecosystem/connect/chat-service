import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';
import { GroupDatabase } from '@kominal/connect-models/group/group.database';
import { Group } from '@kominal/connect-models/group/group';
import Router from '@kominal/service-util/helper/router';
import { verifyParameter } from '@kominal/service-util/helper/util';
import { getMembership, getGroup } from '../../helper';
import { service } from '../..';

const router = new Router();

/**
 * Lists the groups the user is a member of.
 * @group Protected
 * @security JWT
 * @route GET /
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - List of groups
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/groups', async (req, res, userId) => {
	const groups: Group[] = [];

	for (const membership of await MembershipDatabase.find({ userId, removed: null })) {
		const group = await GroupDatabase.findById(membership.get('groupId'));
		if (group == null) {
			continue;
		}

		const g = group.toJSON();

		let p;
		if (g.type == 'DM') {
			const partner = await MembershipDatabase.findOne({ groupId: group._id, userId: { $ne: userId } });
			if (partner == null) {
				continue;
			}
			p = partner.toJSON();
		}

		const m = membership.toJSON();

		groups.push({
			id: g._id,
			name: g.name,
			type: g.type,
			groupKey: m.groupKey,
			joined: m.joined,
			left: m.left,
			role: m.role,
			activityAt: g.activityAt,
			avatar: g.avatar,
			updatedAt: g.updatedAt,
			partnerId: p?.userId,
			partnerProfileId: p?.profileId,
			unreadMessages: m.unreadMessages,
			latestMessageId: m.latestMessageId,
		});
	}

	res.status(200).send(groups);
});

/**
 * Get a specific group
 * @group Protected
 * @security JWT
 * @route GET /{groupId}
 * @consumes application/json
 * @produces application/json
 * @returns {Array} 200 - List of groups
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/group/:groupId', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const m = await getMembership(groupId, userId);

	const group = await GroupDatabase.findById(m.groupId);
	if (group == null) {
		throw 'error.group.notfound';
	}

	const g = group.toJSON();

	let p;
	if (g.type == 'DM') {
		const partner = await MembershipDatabase.findOne({ groupId: group._id, userId: { $ne: userId } });
		if (partner == null) {
			throw 'error.partner.notfound';
		}
		p = partner.toJSON();
	}

	res.status(200).send({
		id: g._id,
		name: g.name,
		type: g.type,
		groupKey: m.groupKey,
		joined: m.joined,
		left: m.left,
		role: m.role,
		activityAt: g.activityAt,
		avatar: g.avatar,
		updatedAt: g.updatedAt,
		partnerId: p?.userId,
		partnerProfileId: p?.profileId,
		unreadMessages: m.unreadMessages,
		latestMessageId: m.latestMessageId,
	});
});

/**
 * Update a group
 * @group Protected
 * @security JWT
 * @route POST /{groupId}
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.path.required - The group id of the group
 * @param {string} name.body.optional - The new name of the group.
 * @returns {void} 200
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/group/:groupId', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const m = await getMembership(groupId, userId);

	if (m.role !== 'ADMIN') {
		throw 'error.group.notadmin';
	}

	const group = await getGroup(groupId);

	if (group.type === 'DM') {
		throw 'error.group.cannotmodifydm';
	}

	let update: any = {};

	const name = req.body.name;
	if (name && name.data && name.iv) {
		update = { name };
	}

	const avatar = req.body.avatar;
	if (avatar) {
		update.avatar = avatar;
	}

	if (Object.keys(update).length > 0) {
		await GroupDatabase.updateOne({ _id: groupId }, update);
	}

	res.status(200).send();

	const userIds = (await MembershipDatabase.find({ groupId })).map((m) => m.get('userId'));

	await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
		headers: {
			userIds,
		},
		type: 'connect.group.updated',
		content: {
			groupId,
		},
	});
});

export default router.getExpressRouter();
