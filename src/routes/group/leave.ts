import { verifyParameter } from '@kominal/service-util/helper/util';
import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import { getGroup, getMembership } from '../../helper';
import { service } from '../..';

const router = new Router();

/**
 * @group Protected
 * @security JWT
 * @route POST /leave
 * @consumes application/json
 * @produces application/json
 * @param {string} group.path.required - The group id
 * @returns {void} 200 - The group was left.
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/group/:groupId/leave', async (req, res, userId) => {
	const { groupId } = req.params;
	verifyParameter(groupId);

	const g = await getGroup(groupId);

	if (g.type === 'DM') {
		throw 'error.group.cannotaddtodm';
	}

	const membership = await getMembership(groupId, userId);

	if (membership.get('left') != null) {
		throw 'error.group.notamember';
	}

	const userIds = (await MembershipDatabase.find({ groupId, left: undefined })).map((m) => m.get('userId'));

	await MembershipDatabase.updateMany({ groupId, userId }, { left: new Date() });

	res.status(200).send();

	await service.getRMQClient().publish('ecosystem.clients.outbound', '', {
		headers: {
			userIds,
		},
		type: 'connect.group.left',
		content: {
			userId,
			groupId,
		},
	});
});

export default router.getExpressRouter();
