import { verifyParameter } from '@kominal/service-util/helper/util';
import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';
import Router from '@kominal/service-util/helper/router';
import { getMembership, getGroup } from '../../helper';

const router = new Router();

/**
 * Changes the role of a group member.
 * @group Protected
 * @security JWT
 * @route POST /role
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.body.required - The id of the group
 * @param {string} partnerId.body.required - The id of the user for which the permission should be changed
 * @param {string} role.body.required - The new role
 * @returns {void} 200 - The role was changed
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/group/:groupId/role/:partnerId/:role', async (req, res, userId) => {
	const { groupId, partnerId, role } = req.params;
	verifyParameter(groupId, partnerId, role);

	if (role != 'ADMIN' && role != 'MEMBER') {
		throw 'error.role.invalid';
	}

	const g = await getGroup(groupId);

	if (g.type === 'DM') {
		throw 'error.group.cannotmodifydm';
	}

	const m = await getMembership(groupId, userId);

	if (m.role !== 'ADMIN') {
		throw 'error.group.notadmin';
	}

	if (
		(await MembershipDatabase.find({ groupId, role: 'ADMIN', left: null, removed: null })).length <= 1 &&
		role != 'ADMIN' &&
		userId === partnerId
	) {
		throw 'error.group.lastAdmin';
	}

	const mPartner = await getMembership(groupId, partnerId);

	if (mPartner == null || mPartner.left != null) {
		throw 'error.group.notamember';
	}

	await MembershipDatabase.updateMany({ groupId, userId: partnerId }, { role });

	res.status(200).send();
});

export default router.getExpressRouter();
