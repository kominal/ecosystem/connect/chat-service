import { verifyParameter } from '@kominal/service-util/helper/util';
import { MessageDatabase } from '@kominal/connect-models/message/message.database';
import Router from '@kominal/service-util/helper/router';
import { service } from '../..';
import { GroupDatabase } from '@kominal/connect-models/group/group.database';
import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';

const router = new Router();

/**
 * Send a message to a group.
 * @group Protected
 * @security JWT
 * @route POST /send
 * @consumes application/json
 * @produces application/json
 * @param {string} groupId.body.required - The group the message should be sent to.
 * @param {string} storageId.body.required - The storage id
 * @returns {void} 200 - TODO
 * @returns {Error}  401 - Unauthorized - Missing authorization header
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.postAsUser('/message', async (req, res, userId) => {
	const { groupId, text, links, attachments } = req.body;
	verifyParameter(groupId, links, attachments);

	const members: string[] = (await MembershipDatabase.find({ groupId, left: null })).map((membership) => String(membership.get('userId')));

	if (members.indexOf(userId) === -1) {
		throw 'error.group.notmember';
	}

	const message = await MessageDatabase.create({
		userId,
		groupId,
		time: Date.now(),
		text,
		links,
		attachments,
	});
	const m = message.toJSON();

	await service.getRMQClient().sendToQueue('ecosystem.notification', {
		userIds: members,
		event: {
			type: 'connect.message.created',
			content: {
				groupId,
				messageId: m._id,
			},
		},
		pushnotification: {
			title: 'Connect',
			body: 'You have a new message!',
			icon: 'assets/images/logo/logo-512x512.png',
			vibrate: [100, 50, 100],
			data: { url: `/connect/group/${groupId}` },
			actions: [{ action: 'open-chat', title: 'Open chat' }],
			renotify: true,
			tag: `MESSAGE_${groupId}`,
		},
	});

	await GroupDatabase.updateOne({ _id: groupId }, { activityAt: m.time });
	await MembershipDatabase.updateMany({ groupId, left: null }, { $inc: { unreadMessages: 1 }, latestMessageId: m._id });

	res.status(200).send({
		id: m._id,
		time: m.time,
	});
});

export default router.getExpressRouter();
