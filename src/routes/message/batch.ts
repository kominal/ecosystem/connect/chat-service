import { verifyParameter } from '@kominal/service-util/helper/util';
import { MessageDatabase } from '@kominal/connect-models/message/message.database';
import Router from '@kominal/service-util/helper/router';
import { Document } from 'mongoose';
import { getMembership } from '../../helper';
import { MembershipDatabase } from '@kominal/connect-models/membership/membership.database';

const router = new Router();

router.getAsUser('/batch/message/:messageId/:currentBatchLatestMessageId*?', async (req, res, userId) => {
	const { messageId, currentBatchLatestMessageId } = req.params;
	verifyParameter(messageId);

	const messageObject = await MessageDatabase.findById(messageId);

	if (!messageObject) {
		throw 'error.message.notfound';
	}

	const m = messageObject.toJSON();

	const member = await getMembership(m.groupId, userId);

	if (member.joined > m.time || (member.left != null && member.left < m.time)) {
		throw 'error.message.notfound';
	}

	const message = {
		id: m._id,
		userId: m.userId,
		time: m.time,
		groupId: m.groupId,
		text: m.text,
		links: m.links,
		attachments: m.attachments,
	};

	let overflow = true;

	if (currentBatchLatestMessageId != null) {
		const currentBatchLatestMessage = await MessageDatabase.findOne({ _id: currentBatchLatestMessageId, groupId: m.groupId });
		if (currentBatchLatestMessage != null) {
			overflow =
				(await MessageDatabase.find({
					$and: [{ time: { $gt: currentBatchLatestMessage.get('time') } }, { time: { $lt: m.time } }],
				}).countDocuments()) > 0;
		}
	}

	res.status(200).send({
		overflow,
		message,
	});
});

router.getAsUser(
	'/batch/messages/:groupId/PREVIOUS/:currentBatchEarliestMessageId*?/:previousBatchLatestMessageId*?',
	async (req, res, userId) => {
		const { groupId, currentBatchEarliestMessageId, previousBatchLatestMessageId } = req.params;
		verifyParameter(groupId);

		const constraints = await buildBaseConstraints(groupId, userId);

		if (currentBatchEarliestMessageId != null && currentBatchEarliestMessageId != '') {
			const currentBatchEarliestMessage = await MessageDatabase.findOne({ _id: currentBatchEarliestMessageId, groupId });
			if (currentBatchEarliestMessage != null) {
				constraints.push({ time: { $lt: currentBatchEarliestMessage.get('time') } });
			}
		}

		if (previousBatchLatestMessageId != null && previousBatchLatestMessageId != '') {
			const previousBatchLatestMessage = await MessageDatabase.findOne({ _id: previousBatchLatestMessageId, groupId });
			if (previousBatchLatestMessage != null) {
				constraints.push({ time: { $gt: previousBatchLatestMessage.get('time') } });
			}
		}

		const count = await MessageDatabase.find({
			$and: constraints,
		}).countDocuments();

		const overflow = count > 1000;
		const messages = convertMessages(
			await MessageDatabase.find({
				$and: constraints,
			})
				.sort({ time: -1 })
				.limit(1000)
		);

		res.status(200).send({
			overflow,
			messages,
		});
	}
);

router.getAsUser('/batch/messages/:groupId/NEXT/:currentBatchLatestMessageId*?', async (req, res, userId) => {
	const { groupId, currentBatchLatestMessageId } = req.params;
	verifyParameter(groupId);

	const constraints = await buildBaseConstraints(groupId, userId);

	if (currentBatchLatestMessageId != null) {
		const currentBatchLatestMessage = await MessageDatabase.findOne({ _id: currentBatchLatestMessageId, groupId });
		if (currentBatchLatestMessage != null) {
			constraints.push({ time: { $gt: currentBatchLatestMessage.get('time') } });
		}
	}

	const count = await MessageDatabase.find({
		$and: constraints,
	}).countDocuments();

	const overflow = currentBatchLatestMessageId && count > 1000;
	const messages = convertMessages(
		await MessageDatabase.find({
			$and: constraints,
		})
			.sort({ time: 1 })
			.limit(1000)
	);

	await MembershipDatabase.updateOne({ userId, groupId }, { unreadMessages: 0 });

	res.status(200).send({
		overflow,
		messages,
	});
});

async function buildBaseConstraints(groupId: string, userId: string): Promise<any[]> {
	const { joined, left } = await getMembership(groupId, userId);

	const conditions: any[] = [];

	conditions.push({ groupId });

	if (joined) {
		conditions.push({ time: { $gt: joined } });
	}

	if (left) {
		conditions.push({ time: { $lt: left } });
	}

	return conditions;
}

function convertMessages(messages: Document[]): any[] {
	return messages
		.map((message) => message.toJSON())
		.map((m) => {
			return {
				id: m._id,
				userId: m.userId,
				time: m.time,
				groupId: m.groupId,
				text: m.text,
				links: m.links,
				attachments: m.attachments,
			};
		});
}

export default router.getExpressRouter();
