import { verifyParameter } from '@kominal/service-util/helper/util';
import Router from '@kominal/service-util/helper/router';
import StorageDatabase from '@kominal/connect-models/storage/storage.database';

const router = new Router();

/**
 * Gets a list of storage objects
 * @group Public
 * @route GET /
 * @consumes application/json
 * @produces application/json
 * @param {string} ids.query.required - A comma sperated list of storageIds
 * @returns {Array} 200 - A list of storage objects
 * @returns {Error}  406 - Not Acceptable - A parameter is missing or is invalid or the request could not be processed
 * @returns {Error}  500 - Internal Server Error
 */
router.getAsUser('/storage', async (req, res) => {
	const ids = req.query.ids as string;
	verifyParameter(ids);
	const results: any[] = [];
	for (const id of ids.split(',')) {
		const data = await StorageDatabase.findById(id);
		if (!data) {
			throw 'error.data.invalid';
		}
		results.push({
			id: data._id,
			userId: data.get('userId'),
			created: data.get('created'),
			data: {
				data: data.get('data'),
				iv: data.get('iv'),
			},
		});
	}
	res.status(200).send(results);
});

export default router.getExpressRouter();
