import Service from '@kominal/service-util/helper/service';
import add from './routes/group/add';
import create from './routes/group/create';
import groups from './routes/group/groups';
import members from './routes/group/members';
import remove from './routes/group/remove';
import leave from './routes/group/leave';
import role from './routes/group/role';
import kick from './routes/group/kick';
import getMessage from './routes/message/get';
import post from './routes/message/post';
import get from './routes/storage/get';
import gridfs from './routes/storage/gridfs';
import save from './routes/storage/save';
import batch from './routes/message/batch';
import { onMessage, onSyncMessage } from './handler.rmq';
import relay from './routes/relay';
import status from './routes/status';
import check from './scheduler/check';

export const service = new Service({
	id: 'chat-service',
	name: 'Chat Service',
	description: 'Manages groups and messages.',
	jsonLimit: '16mb',
	routes: [getMessage, post, get, gridfs, save, add, create, groups, members, remove, leave, role, kick, batch, relay, status],
	database: 'chat-service',
	scheduler: [{ task: check, squad: false, frequency: 30 }],
	rabbitMQ: async (channel) => {
		await channel.assertExchange('ecosystem.clients.outbound', 'fanout', { durable: false });

		await channel.assertExchange('ecosystem.clients.inbound', 'fanout', { durable: false });

		{
			const { queue } = await channel.assertQueue('connect.chat.ecosystem.clients.inbound', { durable: false });
			await channel.bindQueue(queue, 'ecosystem.clients.inbound', '');
			channel.consume(queue, (msg) => onMessage(channel, msg));
		}

		{
			const { queue } = await channel.assertQueue('connect.ecosystem.sync', { durable: false });
			await channel.bindQueue(queue, 'ecosystem.sync', '');
			channel.consume(queue, (msg) => onSyncMessage(channel, msg));
		}

		await channel.assertQueue('ecosystem.notification', { durable: false, messageTtl: 60000 });
	},
});
service.start();
