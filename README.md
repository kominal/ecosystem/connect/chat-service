# Chat Service

The chat service is responsible for keeping track of the members of a group as well as the permissions of the individual users.
Additionally it stores and deliveres messages that have been sent to a group.

## Documentation

Production: https://connect.kominal.com/chat-service/api-docs
Test: https://connect-test.kominal.com/chat-service/api-docs
